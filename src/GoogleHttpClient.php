<?php

namespace Intersect\SDK\GoogleDrive;

use Intersect\SDK\Core\HttpClient;
use Intersect\SDK\Core\HttpRequestData;

class GoogleHttpClient extends HttpClient {

    public function __construct($options)
    {
        $this->httpClient = new \Google_Client();

        if (!is_array($options))
        {
            $this->setAccessToken($options);
        }
        else
        {
            $this->httpClient->setClientId($this->getOptionValue($options, 'client-id'));
            $this->httpClient->setClientSecret($this->getOptionValue($options, 'client-secret'));
            $this->httpClient->setRedirectUri($this->getOptionValue($options, 'redirect-uri'));
            $this->httpClient->setApprovalPrompt($this->getOptionValue($options, 'approval-prompt'));
            $this->httpClient->addScope('https://www.googleapis.com/auth/drive.file');
            $this->httpClient->setAccessType('offline');

/*
            $scopes = $this->getOptionValue($options, 'scopes');
            if (!is_null($scopes))
            {
                if (!is_array($scopes))
                {
                    $scopes = [$scopes];
                }
                $this->httpClient->addScope = $scopes;
            }*/
        }

    }

    /**
     * @param $code
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getAccessTokenDetails($code)
    {
        $this->httpClient->authenticate($code);

        $responseObject = new \stdClass();
        $accessToken = $this->httpClient->getAccessToken();

        $responseObject->access_token = $accessToken['access_token'];
        $responseObject->expires_in = $accessToken['expires_in'];
        $responseObject->refresh_token = $accessToken['refresh_token'];
        $responseObject->scope = $accessToken['scope'];
        $responseObject->token_type = $accessToken['token_type'];

        return $responseObject;
    }

    protected function getBaseAuthorizationEndpoint()
    {
    }

    /**
     * @param $state
     * @param $responseType
     * @return string
     */
    public function generateAuthorizationUrl($state, $responseType = 'code')
    {
        return $this->httpClient->createAuthUrl();
    }

    /**
     * @param $refreshToken
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function refreshAccessToken($refreshToken)
    {
        $responseObject = $this->httpClient->refreshAccessToken($refreshToken);
        return $responseObject->access_token;
    }

    public function createDirectory($accessToken, $metadata, $parameters)
    {
        $drive = new \Google_Service_Drive($this->httpClient);
        $drive->getClient()->setAccessToken($accessToken);

        if (!array_key_exists('mimeType', $metadata) || $metadata['mimeType'] !== 'application/vnd.google-apps.folder')
        {
            $metadata['mimeType'] = 'application/vnd.google-apps.folder';
        }

        $directoryMetadata = new \Google_Service_Drive_DriveFile($metadata);
        $directory = $drive->files->create($directoryMetadata, $parameters);

        return $directory;
    }

    public function createFile($accessToken, $metadata, $parameters)
    {
        $drive = new \Google_Service_Drive($this->httpClient);
        $drive->getClient()->setAccessToken($accessToken);

        $fileMetadata = new \Google_Service_Drive_DriveFile($metadata);
        $file = $drive->files->create($fileMetadata, $parameters);

        return $file;
    }
}
