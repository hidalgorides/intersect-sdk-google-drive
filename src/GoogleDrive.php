<?php

namespace Intersect\SDK\GoogleDrive;

class GoogleDrive {

    /** @var GoogleHttpClient */
    private $httpClient;

    public function __construct($options)
    {
        $this->httpClient = new GoogleHttpClient($options);
    }

    public function setAccessToken($accessToken)
    {
        $this->httpClient->setAccessToken($accessToken);
    }

     /**
     * @param $code
     * @return GoogleDriveAccessTokenDetails
     * @throws \Intersect\SDK\HttpClientException
     */
    public function authenticate($code)
    {
        $responseObject = $this->httpClient->getAccessTokenDetails($code);
        $accessTokenDetails = new GoogleDriveAccessTokenDetails();

        $accessTokenDetails->setAccessToken($responseObject->access_token);
        $accessTokenDetails->setCreated($responseObject->access_token);
        $accessTokenDetails->setExpires($responseObject->expires_in);
        $accessTokenDetails->setRefreshToken($responseObject->refresh_token);
        $accessTokenDetails->setScope($responseObject->scope);
        $accessTokenDetails->setType($responseObject->token_type);

        return $accessTokenDetails;
    }

    /**
     * @param $refreshToken
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function refreshAccessToken($refreshToken)
    {
        $responseObject = $this->httpClient->refreshAccessToken($refreshToken);
        return $responseObject->access_token;
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl()
    {
        $state = null;
        $responseType = 'code';

        return $this->httpClient->generateAuthorizationUrl($state, $responseType);
    }

    public function getAccessTokenDetails()
    {
        return $this->httpClient->getAccessToken();
    }

    public function createDrive()
    {
        return $this->httpClient->createDrive();
    }

    /**
     * @param $accessToken
     * @param $metadata
     * @param $paramters
     * @return Directory
     */
    public function createDirectory($accessToken, $metadata, $parameters)
    {
        return $this->httpClient->createDirectory($accessToken, $metadata, $parameters);
    }

    public function createFile($accessToken, $metadata, $parameters)
    {
        return $this->httpClient->createFile($accessToken, $metadata, $parameters);
    }
}
