<?php

namespace Intersect\SDK\GoogleDrive;

use Intersect\SDK\Core\AccessTokenDetails;

class GoogleDriveAccessTokenDetails extends AccessTokenDetails {

    private $created;
    private $scope;

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        $this->created = $created;
    }

    public function getScope()
    {
        return $this->scope;
    }

    public function setScope($scope)
    {
        $this->scope = $scope;
    }
}
