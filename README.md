# Intersect SDK - Google Drive
**Intersect SDK - Google Drive** contains functionality for integrating with the Google Drive platform

## Changelog
See `CHANGELOG.md` for all current and released features/changes

## Source
https://bitbucket.org/hidalgorides/intersect-sdk-google-drive

## Installation via Composer
Include the following code snippet inside your project `composer.json` file (update if necessary)
```
"repositories": [
  {
    "type": "vcs",
    "url": "https://bitbucket.org/hidalgorides/intersect-sdk-google-drive
  }
],
"require" : {
  "intersect/sdk-google-drive" : "^1.0.0"
}
```

# Usage

__Namespace:__
Intersect\SDK\GoogleDrive

The Google Drive SDK is used for integrating your site with the Google Drive API for retrieving directory contents, creating directories, deleting files, deleting directories, and uploading new files.

## Prerequisites

Prior to using this SDK, you will need to do either of the following steps
- Create an integration token (some features not available when only using an integration token)
- Setup a developer application

## Create a new GoogleDrive instance

```php
<?php

$dropbox = new \Intersect\SDK\Dropbox\Dropbox([
'client-id' => 'INSERT_CLIENT_ID',
'client-secret' => 'INSERT_CLIENT_SECRET',
'redirect-uri' => 'INSERT_REDIRECT_URI'
]);
```

## Obtaining an access token

Generate the authorization URL
```php
<?php

$state = 'INSERT_STATE';
echo $dropbox->getAuthorizationnUrl($state);
```

Once you allow access, you will be redirected back to the redirect URI you set in your Dropbox instance configuration (redirect-uri).

After being redirected, you will have access to the _state_ and _code_ query parameters in the URL. You can use the _state_ value to confirm the request is coming from the state you initial sent with the authorization request. You will need to retrieve the _code_ value from the URL in order to retrieve your new access token details.

Example URL: https://example.com/?state=your-state&code=593fh60hf

Once you have verified the state, if needed, you can proceed to use the _code_ value to obtain your new access token details

```php
<?php

$code = $_GET['code'];

$accessTokenDetails = $dropbox->authenticate($code);

var_dump($accessTokenDetails);
```

You can now use the access token details to to retrieve the actual access token needed to perform further operations. You may store these details however you like to use them for repeated calls, but use your discretion when storing any data.

```php
<?php

$dropbox->setAccessToken($accessTokenDetails->getAccessToken());
```

## Operations

### Creating a directory
```php
<?php

$createdDirectory = $dropbox->createDirectory($rootPath, $directoryName)

var_dump($createdDirectory);
```
